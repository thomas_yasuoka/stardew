Attribute VB_Name = "crops"
Option Explicit

Sub imprt()
    Dim ws As Worksheet, strFile As String

    Set ws = Worksheets("Crops") 'set to current worksheet name
    
    Rem MsgBox "Nesse pr�ximo seletor de arquivos selecione o crops.csv"
    Rem strFile = Application.GetOpenFilename("Text Files (*.csv),*.csv", , "Selectione o crops.csv")
    Rem strFile = "D:/dev/stardew/crops.csv"
    strFile = ThisWorkbook.Path & "/crops.csv"

    With ws.QueryTables.add(connection:="TEXT;" & strFile, Destination:=ws.Range("A1"))
         .TextFileParseType = xlDelimited
         .TextFileCommaDelimiter = True
         .Refresh
    End With
    
    Dim connection As WorkbookConnection
    Dim query As WorkbookQuery
    On Error Resume Next
    For Each connection In ThisWorkbook.Connections
      connection.Delete
    Next
    For Each query In ThisWorkbook.Queries
     query.Delete
    Next
End Sub

Sub img_add(rw, col, link)

    With ActiveSheet.Pictures.Insert(link)
        With .ShapeRange
            .LockAspectRatio = True
            .Width = Cells(rw, col).Width
            .Height = Cells(rw, col).Height
        End With
     .Left = Cells(rw, col).Left + Cells(rw, col).Width - .ShapeRange.Width
     .Top = Cells(rw, col).Top
     .Placement = 1
     .PrintObject = True
    End With
    'msoTrue

End Sub

Sub formatting()
    Dim last_spring, last_summer As Variant
    Dim ln As Integer
    Dim ws As Worksheet
    Dim spring_rng, summer_rng, fall_rns As Range
    Set ws = Worksheets("Crops")
    Dim test, tst, tst1 As Variant
    ln = Cells(Rows.Count, "A").End(xlUp).Row
    
    With ws.Range("A1:E1")
        .Font.Bold = True
        With .Interior
            .Color = 13434726
            .TintAndShade = 0.599993896298105
            .ThemeColor = xlThemeColorAccent1
            .Pattern = xlSolid
            .PatternColorIndex = xlAutomatic
        End With
    End With
    
    With ws.Columns("C")
        .ColumnWidth = .ColumnWidth * 1.5
    End With
    
    With ws.Columns("D")
        .ColumnWidth = .ColumnWidth * 2
    End With
    
    last_spring = "Unmilled_Rice"
    last_summer = "Wheat"
    last_spring = ws.Range(ws.Cells(1, 1), ws.Cells(ln, 3)).Find(last_spring, LookIn:=xlValues).Row
    last_summer = ws.Range(ws.Cells(1, 1), ws.Cells(ln, 3)).Find(last_summer, LookIn:=xlValues).Row
    
    Call img_add(1, 1, "https://stardewvalleywiki.com/mediawiki/images/c/c8/Giant_Pumpkin.png")
    Call img_add(1, 3, "https://stardewvalleywiki.com/mediawiki/images/1/10/Gold.png")
    Call img_add(1, 4, "https://stardewvalleywiki.com/mediawiki/images/1/1e/Preserves_Jar.png")
    Call img_add(1, 5, "https://stardewvalleywiki.com/mediawiki/images/7/7c/Keg.png")
    Range(Cells(2, 3), Cells(ln, 6)).NumberFormat = "$#"
    
    With Range(Cells(2, 1), Cells(last_spring, 5))
        .Borders(xlEdgeBottom).LineStyle = xlContinuous
        .Interior.Pattern = xlSolid
        With .Interior
            .Pattern = xlSolid
            .PatternColorIndex = xlAutomatic
            .Color = 10092441
            .TintAndShade = 0
            .PatternTintAndShade = 0
        End With
    End With
    
    With Range(Cells(last_spring + 1, 1), Cells(last_summer, 5))
        .Borders(xlEdgeBottom).LineStyle = xlContinuous
        With .Interior
            .Pattern = xlSolid
            .PatternColorIndex = xlAutomatic
            .Color = 6750207
            .TintAndShade = 0
            .PatternTintAndShade = 0
        End With
    End With
    
    With Range(Cells(last_summer + 1, 1), Cells(ln, 5))
        .Borders(xlEdgeBottom).LineStyle = xlContinuous
        With .Interior
            .Pattern = xlSolid
            .PatternColorIndex = xlAutomatic
            .Color = 7838659
            .TintAndShade = 0
            .PatternTintAndShade = 0
        End With
    End With
    

    With ActiveSheet.Pictures.Insert("https://stardewvalleywiki.com/mediawiki/images/9/9c/Spring.png")
        With .ShapeRange
            .LockAspectRatio = True
            .Width = 48
            .Height = 32
        End With
     .Left = Cells((last_spring + 2) / 2, 7).Left
     .Top = Cells((last_spring + 2) / 2, 7).Top
     .Placement = 1
     .PrintObject = True
    End With
    
    With ActiveSheet.Pictures.Insert("https://stardewvalleywiki.com/mediawiki/images/8/85/Summer.png")
        With .ShapeRange
            .LockAspectRatio = True
            .Width = 48
            .Height = 32
        End With
     .Left = Cells((last_summer + last_spring + 1) / 2, 7).Left
     .Top = Cells((last_summer + last_spring + 1) / 2, 7).Top
     .Placement = 1
     .PrintObject = True
    End With
    
    With ActiveSheet.Pictures.Insert("https://stardewvalleywiki.com/mediawiki/images/5/5d/Fall.png")
        With .ShapeRange
            .LockAspectRatio = True
            .Width = 48
            .Height = 32
        End With
     .Left = Cells((ln + last_summer + 1) / 2, 7).Left
     .Top = Cells((ln + last_summer + 1) / 2, 7).Top
     .Placement = 1
     .PrintObject = True
    End With
    
'    Call img_add(Round(last_spring / 2), 6, "https://stardewvalleywiki.com/mediawiki/images/9/9c/Spring.png")
'    Call img_add(Round(last_summer + last_summer / 2), 6, "https://stardewvalleywiki.com/mediawiki/images/8/85/Summer.png")
'    Call img_add(Round(last_spring / 2), 6, "https://stardewvalleywiki.com/mediawiki/images/5/5d/Fall.png")
'
    
    Rem Debug.Print last_spring; last_summer; ln
    Rem spring_rng
End Sub

Sub calcs()
    Dim ws As Worksheet
    Dim ln, i As Integer
    Set ws = Worksheets("Crops")
    ln = Cells(Rows.Count, "A").End(xlUp).Row
    ws.Columns("C").Replace "g", ""
    ws.Cells(1, 4) = "Preserves Jar"
    ws.Cells(1, 5) = "Keg"
    For i = 2 To ln
        Cells(i, 4) = Cells(i, 3) * 2 + 50
        If Cells(i, 2) = "Vegetable" Then
            Cells(i, 5) = Cells(i, 3) * 2.25
        ElseIf Cells(i, 2) = "Fruit" Then
            Cells(i, 5) = Cells(i, 3) * 3
        Else
            Cells(i, 5) = "-"
        End If
    Next
End Sub

Sub main()
    Dim ws As Worksheet
    Set ws = Worksheets("Sheet1")
    ws.name = "Crops"
    Call tst
    Call imprt
    Call calcs
    Call formatting
End Sub

Sub tst()


Dim shape As Excel.shape
Cells.Clear

For Each shape In ActiveSheet.Shapes
        shape.Delete
Next


End Sub


