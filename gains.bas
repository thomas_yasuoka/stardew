VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} gains 
   Caption         =   "Gainsss"
   ClientHeight    =   5415
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   5925
   OleObjectBlob   =   "gains.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "gains"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub add_Click()
    Dim ws As Worksheet
    Set ws = Worksheets("temp_order")
    Dim lst_r As Integer
    
    If Len(summary.Value) = 0 Then
        summary = crop_selection.Value + ": " + qtd.Value
        With ws
            lst_r = .Range("A" & .Rows.Count).End(xlUp).Row
        End With
        ws.Cells(lst_r + 1, 1) = crop_selection.Value
        ws.Cells(lst_r + 1, 2) = qtd.Value
    Else
        summary = summary + vbCrLf + crop_selection.Value + ": " + qtd.Value
        With ws
            lst_r = .Range("A" & .Rows.Count).End(xlUp).Row
        End With
        ws.Cells(lst_r + 1, 1) = crop_selection.Value
        ws.Cells(lst_r + 1, 2) = qtd.Value
    End If
End Sub

Private Sub cancel_Click()
    ' deletes temp_order and unloads
    Application.DisplayAlerts = False
    Worksheets("temp_order").Delete
    Application.DisplayAlerts = True
    Unload Me
End Sub

Private Sub submit_Click()
    Dim ord, crp, fmr As Worksheet
    Dim lst_r, lst_crp, lst_fmr, tmp_row As Integer
    Dim i, rng As Range
    Dim test As Range
    Dim farming_lvl, fertilizer_lvl As Integer
    Dim artisan As Boolean
    Dim base, silver, gold, iridium As Double
    Dim cht As Object
    
    Set ord = Worksheets("temp_order")
    Set crp = Worksheets("Crops")
    Set fmr = Worksheets("Farmers")
    
    With ord
        lst_r = .Range("A" & .Rows.Count).End(xlUp).Row
    End With
    
    With crp
        lst_crp = .Range("A" & .Rows.Count).End(xlUp).Row
    End With
    
    With fmr
        lst_fmr = .Range("A" & .Rows.Count).End(xlUp).Row
    End With

    ' fertilizer_lvl = 0 cause lazy
    fertilizer_lvl = 0
    farming_lvl = fmr.Range(fmr.Cells(1, 1), fmr.Cells(lst_fmr, 3)).Find(fmr_id, LookIn:=xlValues).Row
    
    ' getting wheter farmer is artisan
    If fmr.Cells(farming_lvl, 4) = "Yes" Then
        artisan = True
    Else
        artisan = False
    End If
    
    ' getting farmer level
    farming_lvl = fmr.Cells(farming_lvl, 3).Value

    ' determining silver/gold/iridium probabilities
    gold = 0.2 * (farming_lvl / 10) + 0.2 * (fertilizer_lvl) * ((farming_lvl + 2) / 12) + 0.01
    iridium = gold / 2
    silver = 2 * gold
    If silver > 0.75 Then
        silver = 0.75
    End If
    base = 1 - (gold + iridium + silver)
    ' Silver = 1.25
    ' Gold = 1.5
    ' Iridium = 2
    
    For Each i In ord.Range(ord.Cells(2, 1), ord.Cells(lst_r, 1))
        tmp_row = crp.Range(crp.Cells(2, 1), crp.Cells(lst_crp, 3)).Find(i, LookIn:=xlValues).Row
        ord.Cells(i.Row, 3) = crp.Cells(tmp_row, 3)
        
        ord.Cells(i.Row, 5) = Int(ord.Cells(i.Row, 2) * silver)
        ord.Cells(i.Row, 6) = Int(ord.Cells(i.Row, 2) * gold)
        ord.Cells(i.Row, 7) = Int(ord.Cells(i.Row, 2) * iridium)
        ord.Cells(i.Row, 4) = ord.Cells(i.Row, 2) - (ord.Cells(i.Row, 5) + ord.Cells(i.Row, 6) + ord.Cells(i.Row, 7))
    Next
    
    ' charting
    ' A1:A4:C1:G4
    Set cht = ord.Shapes.AddChart2
'    rng = ord.Range(ord.Range(ord.Cells(1, 1), ord.Cells(lst_r, 1)), ord.Range(ord.Cells(1, 4), ord.Cells(lst_r, 7)))
'    cht.Chart.SetSourceData (rng)
    
'    cht.Chart.SetSourceData (ord.Range("A1:A" & lst_r & ":D1:G" & lst_r))
    cht.Chart.SetSourceData (ord.Range("A1:A" & lst_r & ", D1:G" & lst_r))
    cht.Chart.PlotBy = xlColumns
    cht.Chart.PlotBy = xlRows
    cht.Chart.ChartTitle.Text = "Qualidades Esperadas por Crop"
    cht.name = "qualidade_dist"
    
    Call powerpt
    
    ' Deletes temp_order and unloads
    Application.DisplayAlerts = False
    Worksheets("temp_order").Delete
    Application.DisplayAlerts = True
    Unload Me
End Sub

Sub powerpt()
    Dim pptapp As PowerPoint.Application
    Dim pres As PowerPoint.Presentation
    Dim sld As PowerPoint.slide
    Dim ord As Worksheet
    Dim cht As Object
    
    Set cht = Worksheets("temp_order").ChartObjects("qualidade_dist")
    
    Set pptapp = New PowerPoint.Application
    Set pres = pptapp.Presentations.add
    
    Set sld = pres.Slides.add(1, ppLayoutBlank)
    cht.Copy
    sld.Shapes.PasteSpecial (ppPasteEnhancedMetafile)
    
    pptapp.Visible = msoTrue
    pptapp.Activate
    
    cht.Activate
    
End Sub

Private Sub UserForm_Initialize()
    Dim crops As Variant
    Dim ws, ord As Worksheet
    Dim lst_r As Integer
    
    ' gets crops
    Set ws = Worksheets("Crops")
    With ws
        lst_r = .Range("A" & .Rows.Count).End(xlUp).Row
        crops = .Range(ws.Cells(2, 1), ws.Cells(lst_r, 1)).Value
        crop_selection.List = crops
    End With
    
    ' Creates temp_order worksheet
    If Not WorksheetExists("temp_order") Then
        Sheets.add.name = "temp_order"
    Else
        Application.DisplayAlerts = False
        Worksheets("temp_order").Delete
        Application.DisplayAlerts = True
        Sheets.add.name = "temp_order"
    End If
    
    Set ord = Worksheets("temp_order")
    ord.Range("A1") = "Crop"
    ord.Range("B1") = "Quantity"
    ord.Range("C1") = "Base Price"
    ord.Range("D1") = "Base Quantity"
    ord.Range("E1") = "Silver Quantity"
    ord.Range("F1") = "Gold Quantity"
    ord.Range("G1") = "Iridium Quantity"
    
    ' Activates Farmers worksheet so we can see farmer numbers
    Worksheets("Farmers").Activate

End Sub
