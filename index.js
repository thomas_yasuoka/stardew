const { firefox } = require('playwright');

const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const csvWriter = createCsvWriter({
        path: 'D:/dev/stardew/crops.csv',
        header: [
                    {id: 'crop', title: 'Crop'},
                    {id: 'type', title: 'Type'},
                    {id: 'price', title: 'Price'}
                ]
});
// const records = [
//         {name: 'Bob',  lang: 'French, English'},
//         {name: 'Mary', lang: 'English'}
// ];

// csvWriter.writeRecords(records)       // returns a promise
//     .then(() => {
//                 console.log('...Done');
//             });

(async () => {
    const fox = await firefox.launch({ headless: false });
    const page = await fox.newPage();
    await page.goto("https://stardewvalleywiki.com/Crops");
    var grupos = await page.evaluate( () => Array.from( document.querySelectorAll( 'h3' ), element => element.textContent) );
    grupos = grupos.map(Function.prototype.call, String.prototype.trim);
    var crops = grupos.slice(10, -10);
    crops = crops.map(function(x){return x.replace(" ", "_");});
    var data = [];
    //
    // await page.goto("https://stardewcommunitywiki.com/Yam");
    // await page.goto("https://stardewcommunitywiki.com/Cauliflower");
    // var woo;
    // var reg = /[0-9]*g$/

    // woo = await page.innerHTML(`td[id="infoboxdetail"] >> table.no-wrap >> tbody >> tr >> td:has-text("g")`)
    // woo = await page.innerHTML("td:below(:text('Base Price:')) >> span[data-sort-value='175'] >> td:text('g')")
    //
    // woo = await page.innerHTML("td:below(:text('Base Price:')) >> span[data-sort-value] >> td:text('g')")

    // woo = await page.innerHTML("span:below(:text('Base Price:'))")
    //console.log(woo)
    // woo = await page.innerHTML("[id='infoboxdetail']");
    // woo = await page.innerHTML("xpath=/html/body/div[3]/div[2]/div[6]/div[4]/div[1]/table/tbody/tr[13]/td[1]/span/table/tbody/tr[1]/td[2]");
    // console.log(woo);
    

    for (i = 0; i < crops.length; i++) {
        console.log(`${crops[i]}`);
        await page.goto(`https://stardewvalleywiki.com/${crops[i]}`);
        const flower_check = await page.$$("[alt=Honey]");
        const veggie_check = await page.$$("[alt=Pickles]");
        const fruit_check = await page.$$("[alt=Wine]");
        var what;
        var price;
        if (flower_check.length) {
            console.log("FLOWER");
            what = "Flower";
        } else if (veggie_check.length) {
            console.log("VEGGIE");
            what = "Vegetable";
        } else if (fruit_check.length) {
            console.log("FRUIT");
            what = "Fruit";
        } else {
            console.log("Something else");
            what = "Wot";
        }
        price = await page.innerText('text=/[0-9]+g/i');
        console.log(price);

        // price = await page.getAttribute("td[data-sort-value]", "data-sort-value")
        // const html = await page.$eval("text=/[1-9]*g$/", (e, suffix) => e.outerHTML + suffix, 'hello');
        // console.log(html)
        console.log(price);
        data.push({crop: crops[i], type: what, price: price})
        console.log();
    }
    await csvWriter.writeRecords(data)       // returns a promise
        .then(() => {
                    console.log('...Done');
                });
    console.log(crops);
    await fox.close();
})();
