Attribute VB_Name = "utilities"
Option Explicit

Public Sub export()
    Dim proj
    Dim objVBComp

    Set proj = Application.VBE.ActiveVBProject
    
    For Each objVBComp In proj.VBComponents
        Debug.Print (objVBComp.name)
        If objVBComp.name = "crops" Then
            objVBComp.export ThisWorkbook.Path & "/" & objVBComp.name & ".bas"
        ElseIf objVBComp.name = "userforms" Then
            objVBComp.export ThisWorkbook.Path & "/" & objVBComp.name & ".bas"
        ElseIf objVBComp.name = "utilities" Then
            objVBComp.export ThisWorkbook.Path & "/" & objVBComp.name & ".bas"
        ElseIf objVBComp.name = "farmer" Then
            objVBComp.export ThisWorkbook.Path & "/" & objVBComp.name & ".bas"
        ElseIf objVBComp.name = "gains" Then
            objVBComp.export ThisWorkbook.Path & "/" & objVBComp.name & ".bas"
        End If
    Next
End Sub

Public Function WorksheetExists(shtName As String, Optional wb As Workbook) As Boolean
    Dim sht As Worksheet

    If wb Is Nothing Then Set wb = ThisWorkbook
    On Error Resume Next
    Set sht = wb.Sheets(shtName)
    On Error GoTo 0
    WorksheetExists = Not sht Is Nothing
End Function

Sub test()
    Application.DisplayAlerts = False
    Charts.Delete
    Application.DisplayAlerts = True
End Sub

