VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} farmer 
   Caption         =   "UserForm1"
   ClientHeight    =   3015
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   4560
   OleObjectBlob   =   "farmer.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "farmer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub farming_lvl_Change()
    If farming_lvl.Value = 10 Then
        artisan.Enabled = True
        artisan.Visible = True
        artisan.Value = "No"
        
        artisan_txt.Enabled = True
        artisan_txt.Visible = True
    Else
        artisan.Enabled = False
        artisan.Visible = False
        artisan.Value = "No"
        
        artisan_txt.Enabled = False
        artisan_txt.Visible = False
    End If
End Sub

Private Sub submit_Click()
    Dim ws As Worksheet
    Set ws = Worksheets("Farmers")
    Dim lst_r As Integer
    Dim test As Variant
    
    With ws
        lst_r = .Range("A" & .Rows.Count).End(xlUp).Row
    End With
    
    If TypeName(ws.Cells(lst_r, 1).Value) = "Integer" Or TypeName(ws.Cells(lst_r, 1).Value) = "Double" Then
        ws.Cells(lst_r + 1, 1) = ws.Cells(lst_r, 1) + 1
    ElseIf lst_r = 0 Then
        ws.Cells(lst_r, 1) = 1
    Else
        ws.Cells(lst_r + 1, 1) = 1
    End If
    
    ws.Cells(lst_r + 1, 2) = farmer_name.Value
    ws.Cells(lst_r + 1, 3) = farming_lvl.Value
    ws.Cells(lst_r + 1, 4) = artisan.Value
    Unload Me
End Sub

Private Sub UserForm_Initialize()
    Dim fmr As Worksheet

    If Not WorksheetExists("Farmers") Then
        Sheets.add.name = "Farmers"
        Set fmr = Worksheets("Farmers")
        fmr.Range("A1") = "Farmer N" + ChrW(8304)
        fmr.Range("B1") = "Nome"
        fmr.Range("C1") = "N�vel de Fazendeiro"
        fmr.Range("D1") = "Artes�o?"
    End If
    
    
    farming_lvl.List = Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    
    artisan_txt.Enabled = False
    artisan_txt.Visible = False
    artisan.List = Array("No", "Yes")
    artisan.Value = "No"
    artisan.Enabled = False
    artisan.Visible = False
End Sub

